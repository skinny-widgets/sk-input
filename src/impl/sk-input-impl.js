

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';
import { SkRequired } from "../../../sk-form-validator/src/validators/sk-required.js";
import { SkMax } from "../../../sk-form-validator/src/validators/sk-max.js";
import { SkEmail } from "../../../sk-form-validator/src/validators/sk-email.js";
import { SkPattern } from "../../../sk-form-validator/src/validators/sk-pattern.js";
import { SkMin } from "../../../sk-form-validator/src/validators/sk-min.js";



export var FORWARDED_ATTRS = {
    'name': 'name', 'value': 'value', 'type': 'type',
    'disabled': 'disabled', 'list': 'list', 'placeholder': 'placeholder',
    'minlength': 'minlength', 'maxlength': 'maxlength', 'required': 'required',
    'pattern': 'pattern',
    'min': 'min', 'max': 'max', 'step': 'step',
};

export class SkInputImpl extends SkComponentImpl {

    get suffix() {
        return 'input';
    }

    get input() {
        if (! this._input) {
            this._input = this.comp.el.querySelector('input');
        }
        return this._input;
    }

    set input(el) {
        this._input = el;
    }

    get inputEl() {
        if (! this._inputEl) {
            this._inputEl = this.comp.el.querySelector('input');
        }
        return this._inputEl;
    }

    set inputEl(el) {
        this._inputEl = el;
    }

    get subEls() {
        return [ 'inputEl', 'input' ];
    }

    onInput(event) {
        this.comp.setAttribute('value', event.target.value);
        let result = this.validateWithAttrs();
        if (typeof result === 'string') {
            this.input.setCustomValidity(result);
            this.showInvalid();
        } else {
            this.input.setCustomValidity('');
            this.showValid();
        }
        this.comp.dispatchEvent(new CustomEvent('input', { target: this.comp, detail: { value: event.target.value }, bubbles: true, composed: true }));
        this.comp.dispatchEvent(new CustomEvent('skinput', { target: this.comp, detail: { value: event.target.value }, bubbles: true, composed: true }));
    }

    onFocus(event) {
        this.removeUntouched();
    }

    removeUntouched() {
        if (this.input && this.input.classList.contains('untouched')) {
            this.input.classList.remove('untouched');
        }
    }

    setUntouched() {
        if (this.input) {
            this.input.classList.add('untouched');
        }
    }

    bindEvents() {
        super.bindEvents();
        this.input.oninput = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onInput(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
        this.input.onfocus = function(event) {
            this.comp.callPluginHook('onEventStart', event);
            this.onFocus(event);
            this.comp.callPluginHook('onEventEnd', event);
        }.bind(this);
    }

    unbindEvents() {
        super.unbindEvents();
        this.input.oninput = null;
        this.input.onfocus = null;
    }

    get skValidators() {
        if (! this._skValidators) {
            this._skValidators = {
                'sk-required': new SkRequired(),
                'sk-min': new SkMin(),
                'sk-max': new SkMax(),
                'sk-email': new SkEmail(),
                'sk-pattern': new SkPattern(),
            };
        }
        return this._skValidators;
    }

    showInvalid() {
        super.showInvalid();
        this.inputEl.classList.add('form-invalid');
    }

    showValid() {
        super.showValid();
        this.inputEl.classList.remove('form-invalid');
    }

    restoreState(state) {
        super.restoreState(state);
        this.clearAllElCache();
        this.forwardAttributes();
        this.bindEvents();
    }

    forwardAttributes() {
        for (let attrName of Object.keys(FORWARDED_ATTRS)) {
            let value = this.comp.getAttribute(attrName);
            if (value) {
                this.input.setAttribute(attrName, value);
            } else {
                if (this.comp.hasAttribute(attrName)) {
                    this.input.setAttribute(attrName, '');
                }
            }
            if (attrName === 'list' && value && this.comp.useShadowRoot) { // import datalist into shadow root
                let listEl = document.getElementById(value);
                if (listEl) {
                    this.comp.el.append(document.importNode(listEl, true));
                }
            }
        }
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState();
        this.setUntouched();
    }

    get msgEl() {
        return this.comp.el.querySelector('.form-validation-message');
    }

    renderValidation(validity) {
        validity = validity ? validity : this.comp.inputEl.validationMessage;
        this.comp.inputEl.setCustomValidity(validity);
        super.renderValidation(validity);
    }

    flushValidation() {
        super.flushValidation();
        this.comp.inputEl.setCustomValidity('');
    }

    focus() {
        setTimeout(() => {
            this.comp.inputEl.focus();
        }, 0);
    }
}