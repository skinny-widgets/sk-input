

import { SkElement } from '../../sk-core/src/sk-element.js';

import { FORWARDED_ATTRS } from './impl/sk-input-impl.js';

export class SkInput extends SkElement {

    get cnSuffix() {
        return 'input';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set value(value) {
        if (value === null) {
            this.removeAttribute('value');
        } else {
            this.setAttribute('value', value);
        }
    }

    get value() {
        return this.getAttribute('value');
    }

    set impl(impl) {
        this._impl = impl;
    }

    get subEls() {
        return [ 'inputEl' ];
    }

    get inputEl() {
        if (! this._inputEl && this.el) {
            this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
    }

    set inputEl(el) {
        this._inputEl = el;
    }

    get validity() {
        return this.inputEl ?  this.inputEl.validity : false;
    }

    get validationMessage() {
        return this.inputEl && this.inputEl.validationMessage
            && this.inputEl.validationMessage !== 'undefined' ? this.inputEl.validationMessage : this.locale.tr('Field value is invalid');
    }

    set validationMessage(validationMessage) {
        this._validationMessage = validationMessage;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (! this.inputEl) {
            return false;
        }
        if (FORWARDED_ATTRS[name] && oldValue !== newValue) {
            if (newValue !== null && newValue !== undefined) {
                this.inputEl.setAttribute(name, newValue);
            } else {
                this.inputEl.removeAttribute(name);
            }
        }
        if (name === 'value' && this.inputEl) {
            this.inputEl.value = newValue;
        }
        if (name === 'form-invalid') {
            if (newValue !== null) {
                this.impl.showInvalid();
            } else {
                this.impl.showValid();
            }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    setCustomValidity(validity) {
        super.setCustomValidity(validity);
        this.inputEl.setCustomValidity(validity);
    }

    static get observedAttributes() {
        return ['form-invalid', ...Object.keys(FORWARDED_ATTRS)];
    }

}
